@extends('layouts.app')

@section('title', 'Bảng biểu -')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Bảng biểu</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th class="text-center">Kí hiệu</th>
                        <th class="text-center">Giá trị</th>
                        <th class="text-center">Trạng thái</th>
                      </thead>
                      <tbody>
                      @foreach ($sensorInfos as $sensorInfo)
						@if(isset($sensorValues[$sensorInfo->Sign]))
                        <tr>
                        <td class="text-center">
                            {{$sensorInfo->Sign}}</br>
                            <a href="{{url('/lich-su-du-lieu/'. $sensorInfo->Sign)}}"><span class="detail">Chi tiết</span></a>
                        </td>
                          <td class="text-center">
                            <span style="font-size: .7rem;">Max: {{$sensorInfo->UpLimit}}</span></br>
								{{ $sensorValues[$sensorInfo->Sign] }} {{$sensorInfo->Unit}}</br>
                            <span style="font-size: .7rem;">Min: {{$sensorInfo->DownLimit}}</span>
                          </td>
						  @if($sensorValues['status'][$sensorInfo->Sign])
                          <td class="text-primary text-center">Bình thường</td>
					      @else
					      <td class="text-warning text-center"><strong>CẢNH BÁO!</strong></td>
					      @endif
                        </tr>
						@endif
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection