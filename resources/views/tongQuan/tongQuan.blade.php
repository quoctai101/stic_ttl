@extends('layouts.app')

@section('title', $type['name'] . ' -')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">{{$type['name']}}</h4>
                </div>
                <div class="card-body">
                    <div class="row justify-content-md-center">
                        <div class="col-12 general">
                            <img src="{{asset('img/tongQuan/'. str_replace(' ', '-', strtolower($type['query'])) . '.jpg' )}}" class="img-fluid image-zoom" alt="{{$type['name']}}">
                        </div>
                        @foreach($type['section'] as $section)
                        @php $filename = str_replace(' ', '-', strtolower($section)); @endphp
                        <div class="col-12 col-md-4 section">
                            <a href="#" data-section="{{$section}}">
                                <img src="{{asset('img/tongQuan/'. $filename .'.jpg')}}" class="img-fluid" alt="{{$section}}">
                            </a>
                        </div>
                        @endforeach
                    </div>
                    <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection