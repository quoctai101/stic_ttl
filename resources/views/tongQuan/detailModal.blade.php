<div class="modal-header">
    <h5 class="modal-title">{{$sectionData['title']}}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="table-responsive">
        <table class="table">
            <thead class=" text-primary">
                <th class="text-center">Tên</th>
                <th class="text-center">Giá trị ({{$sectionData['data'][0]->Unit}})</th>
                <th class="text-center">Trạng thái</th>
            </thead>
            <tbody>
                @foreach($sectionData['data'] as $sensor)
                <tr>
                    <td class="text-center">
                        {{$sensor->Sign}}</br>
                        <a href="{{url('/lich-su-du-lieu/'. $sensor->Sign)}}"><span class="detail">Chi tiết</span></a>
                    </td>
                    <td class="text-center">
                        <span style="font-size: .7rem;">Max: {{$sensor->UpLimit}}</span></br>
                        {{$sensor->Value}} {{$sensor->Unit}}</br>
                        <span style="font-size: .7rem;">Min: {{$sensor->DownLimit}}</span>    
                    </td>
                    <td class="text-primary text-center">Bình thường</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>