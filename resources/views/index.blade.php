@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Hình ảnh tổng quan</h4>
                </div>
                <div class="card-body text-center">
                    <img src="{{asset('img/cau-tran-thi-ly.jpg')}}" class="img-fluid image-zoom" alt="Cầu Trần Thị Lý">
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Google Maps</h4>
                </div>
                <div class="card-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.3146258736024!2d108.22403823300085!3d16.049155009395676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfb4224a36e5874d4!2zQ-G6p3UgVHLhuqduIFRo4buLIEzDvQ!5e0!3m2!1svi!2s!4v1592043656745!5m2!1svi!2s" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Map 4D</h4>
                </div>
                <div class="card-body">
                    <iframe src="https://map.map4d.vn/@16.049910,108.229700,18.00,50.0,182.0,1?type=detail&search=C%E1%BA%A7u+Tr%E1%BA%A7n+Th%E1%BB%8B+L%C3%BD&data=5c9845e4814a9df9d67e5a35" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection