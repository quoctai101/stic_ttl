@extends('layouts.app')

@section('title',  'Lỗi -')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Lỗi!</h4>
                </div>
                <div class="card-body">
                <span>Không tìm thấy trang này!</span>
                </div>
            </div>
        </div>
    </div>
@endsection