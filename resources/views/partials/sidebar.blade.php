<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('img/sidebar-1.jpg')}}">
    <div class="logo">
        <a href="{{ url('/') }}" class="simple-text logo-normal">
            Cầu Trần Thị Lý
        </a>
        <a href="{{ url('/') }}" class="simple-text logo-normal">
            Thành phố Đà Nẵng
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav sidebar-nav">
            <li class="nav-item dropdown {{ (request()->is('tong-quan/*')) ? 'active' : '' }}">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">dashboard</i>
                    <p>Tổng quan</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Loadcell</a>
                    <a class="dropdown-item" href="#">Kết cấu (GĐ1)</a>
                    <a class="dropdown-item" href="#">Nhiệt độ (GĐ1)</a>
                    <a class="dropdown-item" href="{{ url('/tong-quan/ket-cau-gd2') }}">Kết cấu (GĐ2)</a>
                    <a class="dropdown-item" href="{{ url('/tong-quan/nhiet-do-gd2') }}">Nhiệt độ (GĐ2)</a>
                    <a class="dropdown-item" href="{{ url('/tong-quan/gia-toc') }}">Gia tốc</a>
                    <a class="dropdown-item" href="#">Gió</a>
                    <a class="dropdown-item" href="#">Chuyển vị đỉnh tháp</a>
                </div>
            </li>
            <li class="nav-item {{ (request()->is('bang-bieu')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/bang-bieu') }}">
                <i class="material-icons">content_paste</i>
                <p>Bảng biểu</p>
                </a>
            </li>
            <li class="nav-item {{ (request()->is('lich-su-du-lieu/*') || request()->is('lich-su-du-lieu')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/lich-su-du-lieu') }}">
                <i class="material-icons">library_books</i>
                <p>Lịch sử dữ liệu</p>
                </a>
            </li>
            <li class="nav-item active-pro ">
                <a class="nav-link text-center" href="#">
                    <img src="{{asset('img/logo.jpg')}}" class="img-fluid image-zoom" width="50px" alt="STIC">
                    <p>Version 3.0</p>
                    <p>Copyright ©2018-2025</p>
                    <p>All rights reserved | This template is made by STIC</p>
                </a>
            </li>
        </ul>
    </div>
</div>