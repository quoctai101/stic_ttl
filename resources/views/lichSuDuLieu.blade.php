@extends('layouts.app')

@section('title', 'Lịch sử bảng biểu - ')

@section('content')
    <div class="row">
      <div class="col-12 col-md-4 order-md-2">
        <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label>Chọn cảm biến</label>
                    <select id="Sign" class="form-control">
                      @foreach($sensors as $sensor)
                      <option value="{{$sensor->Sign}}">{{$sensor->Sign}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <label>Từ ngày</label>
                    <input class="form-control" type="date" name="fromDate">
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <label>Đến ngày</label>
                    <input class="form-control" type="date" name="toDate">
                  </div>
                </div>
              </div>
              <button id="reNewChart" class="btn btn-primary btn-round pull-right">Làm mới</button>
            </div>
        </div>
      </div>
      <div class="col-12 col-md-8 order-md-1">
          <div class="card">
              <div class="card-header card-header-primary">
                  <h4 class="card-title ">Biểu đồ</h4>
              </div>
              <div id="chart-container" class="card-body">
                <canvas id="chart"></canvas>
                @if($sensorData)
                <script type="text/javascript">
                  $(document).ready( function () {
                    var sensorData = JSON.parse('{!!json_encode($sensorData)!!}');
                    if($('#chart').length)
                    {
                        $('#chart').remove();
                    }
                    $('#chart-container').append('<canvas style="width: 100% !important;" height="400" id="chart"></canvas>');
                    var ctx = document.getElementById('chart').getContext('2d');
                    var myLineChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: sensorData.labels,
                            datasets: [{
                                label: sensorData.sign,
                                backgroundColor: '#7198ff',
                                borderColor: '#7198ff',
                                data: sensorData.data,
                                fill: false
                            }]
                        },
                        options: {
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            scales: {
                                xAxes: [{
                                  ticks: {
                                    autoSkip: false,
                                    callback: function(value, index, values) {
                                      if(index == 0 || index == sensorData.labels.length - 1) return value;
                                      else return '';
                                    }
                                  }
                                }]
                            },
                            elements: {
                                line :{
                                    borderWidth: 1
                                },
                                point:{
                                    radius: 0
                                }
                            }
                        }
                    });
                  });
                </script>
                @endif
              </div>
          </div>
      </div>
    </div>
@endsection