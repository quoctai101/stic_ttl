function initChart(sign, data, labels) {
    if ($('#chart').length) {
        $('#chart').remove();
    }

    $('#chart-container').append('<canvas style="width: 100% !important;" height="400" id="chart"></canvas>');
    var ctx = document.getElementById('chart').getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: sign,
                backgroundColor: '#7198ff',
                borderColor: '#7198ff',
                data: data,
                fill: false
            }]
        },
        options: {
            tooltips: {
                mode: 'index',
                intersect: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        callback: function callback(value, index, values) {
                            if (index == 0 || index == labels.length - 1) return value; else return '';
                        }
                    }
                }]
            },
            elements: {
                line: {
                    borderWidth: 1
                },
                point: {
                    radius: 0
                }
            }
        }
    });
}

function getUrl() {
    var slug = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return window.location.origin ? window.location.origin + '/' + slug : window.location.protocol + '/' + window.location.host + '/' + slug;
}

$(document).ready(function () {
    $('.image-zoom').each(function () {
        $(this).viewer({
            toolbar: false,
            viewed: function viewed() {
                $(this).viewer('zoomTo', 1);
            }
        });
    });
    $('#reNewChart').click(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: getUrl('ajaxNewData'),
            method: "POST",
            data: {
                sign: $('#Sign option:selected').val(),
                fromDate: $('input[name=fromDate]').val(),
                toDate: $('input[name=toDate]').val() + " 23:59:59"
            },
            success: function success(response) {
                initChart(response.sign, response.data, response.labels);
            }
        });
    });
    $('div.section a').click(function () {
        $section = $(this).data('section');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: getUrl('ajaxSectionData'),
            method: "POST",
            data: {
                section: $section
            },
            success: function success(response) {
                console.dir(response);
                $('#detailModal .modal-content').html(response);
                $('#detailModal').modal('show');
            }
        });
    });
});