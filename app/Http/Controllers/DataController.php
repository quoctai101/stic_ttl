<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    // Views
    public function getLatestRecord()
    {
        $sensorInfos = DB::table('ttl_sensor')
                        ->get();
		$sensorValues = [];
		$sensorValues['status'] = [];
		foreach($sensorInfos as $sensorInfo)
		{
			$value = DB::table('ttl_recordLine')->select('Value')
						->where('SensorID', '=', $sensorInfo->ID)
						->take(1)->orderBy('DateTime', 'DESC')
						->get();
			if(isset($value[0]->Value)) $sensorValues[$sensorInfo->Sign] = $value[0]->Value;
			if($value[0]->Value > $sensorInfo->UpLimit || $value[0]->Value < $sensorInfo->DownLimit)
				$sensorValues['status'][$sensorInfo->Sign] = 0;
			else $sensorValues['status'][$sensorInfo->Sign] = 1;
		}
        return view('bangBieu', ['sensorInfos' => $sensorInfos, 'sensorValues' => $sensorValues]);
    }
    public function getTongQuan($slug = 'ket-cau-gd2')
    {
        $typeUni = array(
            'ket-cau-gd2' => array(
                'name' => "Kết cấu GĐ2",
                'query' => "Ket cau GD2"
            ),
            'nhiet-do-gd2' => array(
                'name' => "Nhiệt độ GĐ2",
                'query' => "Nhiet do GD2"
            ),
            'gia-toc' => array(
                'name' => "Gia tốc",
                'query' => "Gia toc"
            )
        );
        if(!isset($typeUni[$slug])) return redirect('loi');
        $type = $typeUni[$slug];
        $type['section'] = [];
        $sections = DB::table('ttl_sensor')
                        ->distinct()->select('Des')
                        ->where('Des', "LIKE", $type['query'] . "%")
                        ->get();
        foreach($sections as $section)
        {
            $type['section'][] = $section->Des;
        }
        return view('tongQuan.tongQuan', ['type' => $type]);
    }
    public function lichSuDuLieu($sign = '')
    {
        $sensorData = [];
        if($sign) $sensorData = $this->getSensorData($sign);
        $sensors = DB::table('ttl_sensor')
                        ->select('Sign')
                        ->get();
        return view('lichSuDulieu', ['sensors' => $sensors, 'sensorData' => $sensorData]);
    }
    // Utilities
    public function getSensorData($sign, $fromDate = '' , $toDate = '')
    {
        $result = [];
        if(!$fromDate)
            $db = DB::table('ttl_recordLine')
                    ->leftJoin('ttl_sensor', 'ttl_recordLine.SensorID', '=', 'ttl_sensor.ID')
                    ->select('ttl_recordLine.Value', 'ttl_recordLine.DateTime')
                    ->where('ttl_sensor.Sign', $sign)
                    ->take(100)
                    ->orderBy('DateTime', 'DESC');
        else
            $db = DB::table('ttl_recordLine')
                    ->leftJoin('ttl_sensor', 'ttl_recordLine.SensorID', '=', 'ttl_sensor.ID')
                    ->select('ttl_recordLine.Value', 'ttl_recordLine.DateTime')
                    ->where('ttl_sensor.Sign', $sign)
                    ->whereBetween('ttl_recordLine.DateTime', [$fromDate, $toDate])
                    ->orderBy('DateTime', 'DESC');
        if($db->count())
        {
            $rows = $db->get();
            $result['sign'] = $sign;
            $result['data'] = [];
            $result['labels'] = [];
            foreach($rows as $row)
            {
                $result['data'][] = $row->Value;
                $result['labels'][] = $row->DateTime;
            }
        }
        return $result;
    }
    public function getSectionData($section)
    {
        $result = [];
        $db = DB::table('ttl_sensor')
                ->rightJoin('ttl_recordline', 'ttl_sensor.ID', '=', 'ttl_recordline.SensorID')
                ->where("Des", '=', $section)
                ->groupBy('ttl_sensor.ID');
        if($db->count())
        {
            $result['title'] = $section;
            $result['data'] = $db->get();
        }
        return $result;
    }
    // API
    public function ajaxNewData(Request $request)
    {
        if($request->input('sign'))
        {
            $sign = $request->input('sign');
            $fromDate = $request->input('fromDate');
            $toDate = $request->input('toDate');
            $sensorData = $this->getSensorData($sign, $fromDate, $toDate);
            return response()->json([
                'sign' => $sensorData['sign'],
                'data' => $sensorData['data'],
                'labels' => $sensorData['labels']
            ]);
        }
    }
    public function ajaxSectionData(Request $request)
    {
        if($request->input('section'))
        {
            $sectionData = $this->getSectionData($request->input('section'));
            return view('tongQuan.detailModal', ['sectionData' => $sectionData]);
        }
    }
}
